const r404 = () => import("./components/main-routes/404.vue");
const rHome = () => import("./components/main-routes/home.vue");
const rTypography = () => import("./components/main-routes/typography.vue");
const rForms = () => import("./components/main-routes/forms.vue");
const rComponents = () => import("./components/main-routes/components.vue");

const routes = [
  { path: "*", component: r404, meta: { title: "Page Not Found" } },
  {
    path: "/",
    component: rHome,
    meta: { title: "Home" }
  },
  {
    path: "/typography",
    component: rTypography,
    meta: { title: "Typography" }
  },
  {
    path: "/forms",
    component: rForms,
    meta: { title: "Forms" }
  },
  {
    path: "/components",
    component: rComponents,
    meta: { title: "Components" }
  }
];

export default routes;
