import Vue from "vue";

const vCard = () => import("./components/reusable/card.vue");
Vue.component("v-card", vCard);

const vBadge = () => import("./components/reusable/badge.vue");
Vue.component("v-badge", vBadge);
