var path = require("path");
var webpack = require("webpack");
var SWPrecacheWebpackPlugin = require("sw-precache-webpack-plugin");

const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

const extractGlobalScss = new ExtractTextPlugin({
  filename: "global-[contenthash].css",
  disable: process.env.NODE_ENV === "development"
});
const extractComponentScss = new ExtractTextPlugin({
  filename: "components-[contenthash].css",
  disable: process.env.NODE_ENV === "development"
});

const extractVendor = new webpack.optimize.CommonsChunkPlugin({
  name: "vendor",
  minChunks: function(module) {
    return module.context && module.context.includes("node_modules");
  }
});

const extractRuntime = new webpack.optimize.CommonsChunkPlugin({
  name: "runtime",
  minChunks: Infinity
});

module.exports = {
  entry: "./src/main.js",
  output: {
    path: path.resolve(__dirname, "./dist"),
    publicPath: "/",
    filename:
      "production" === process.env.NODE_ENV
        ? "[name]-[chunkhash].js"
        : "[name].js"
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: "vue-loader",
        options: {
          loaders: {
            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this nessessary.
            // 'scss': 'vue-style-loader!css-loader!sass-loader',
            // 'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
            scss: extractComponentScss.extract({
              use: ["css-loader", "sass-loader"],
              fallback: "vue-style-loader" // <- this is a dep of vue-loader, so no need to explicitly install if using npm3
            })
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.css$/,
        loader: ["style-loader", "css-loader"]
      },
      {
        test: /\.scss$/,
        use: extractGlobalScss.extract({
          use: [
            {
              loader: "css-loader"
            },
            {
              loader: "sass-loader"
            }
          ],
          // use style-loader in development
          fallback: "style-loader"
        })
      },
      {
        test: /\.js$/,
        loader: "babel-loader",
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]?[hash]"
        }
      },
      {
        test: /manifest.json$/,
        loader: "file-loader?name=manifest.json!web-app-manifest-loader"
      }
    ]
  },
  plugins: [
    new webpack.HashedModuleIdsPlugin(),
    extractVendor,
    extractRuntime,
    extractGlobalScss,
    extractComponentScss,
    new HtmlWebpackPlugin({
      template: "index.html",
      minify: {
        collapseWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true
      }
    }),
    new SWPrecacheWebpackPlugin({
      cacheId: "pwa-skeleton",
      dontCacheBustUrlsMatching: /\.\w{8}\./,
      filename: "service-worker.js",
      minify: true,
      navigateFallback: "/",
      staticFileGlobsIgnorePatterns: [/\.map$/, /asset-manifest\.json$/],
      runtimeCaching: [
        {
          urlPattern: /^https:\/\/fonts\.googleapis\.com\//,
          handler: "networkFirst"
        },
        {
          urlPattern: /^https:\/\/fonts\.gstatic\.com\//,
          handler: "networkFirst"
        },
        {
          urlPattern: /^https:\/\/gitlab\.com\//,
          handler: "networkFirst"
        },
        {
          urlPattern: /^https:\/\/unsplash\.it\//,
          handler: "networkFirst"
        }
      ]
    })
  ],
  resolve: {
    alias: {
      vue$: "vue/dist/vue.common.js",
      "vue-router$": "vue-router/dist/vue-router.common.js",
      "global-css": path.resolve(__dirname, "./src/assets/global-css/"),
      img: path.resolve(__dirname, "./src/assets/img/")
    },
    modules: [path.resolve(__dirname, "src"), "node_modules"]
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true
  },
  performance: {
    hints: false
  },
  devtool: "#eval-source-map"
};

if (process.env.NODE_ENV === "production") {
  module.exports.devtool = "#source-map";
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: '"production"'
      }
    }),
    new CleanWebpackPlugin(["dist/*.*"]),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ]);
}
